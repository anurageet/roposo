//
//  RetrieveJson.m
//  demoTEST
//
//  Created by Anurag Bhakuni on 5/3/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import "DDData.h"

@implementation DDData
-(NSArray *)reteriveData
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"myData.json"];
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:appFile encoding:NSUTF8StringEncoding error:NULL];
    NSError *error;
 return  [NSJSONSerialization JSONObjectWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
}
-(void)saveData:(NSArray *)data
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"myData.json"];
    [jsonString writeToFile:appFile atomically:YES encoding:NSUTF8StringEncoding error:nil];

}
@end
