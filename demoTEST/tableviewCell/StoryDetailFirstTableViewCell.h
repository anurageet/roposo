//
//  StoryDetail1TableViewCell.h
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/29/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Story.h"
#import "User.h"
#import "BtnSubclass.h"
@interface StoryDetailFirstTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblHandler;
@property (strong, nonatomic) IBOutlet UILabel *lblFolwers;
@property (strong, nonatomic) IBOutlet BtnSubclass *btnLike;

@property (strong, nonatomic) IBOutlet BtnSubclass *btnFollow;


- (IBAction)btnFollow:(id)sender;
- (IBAction)btnLike:(id)sender;





@property (strong, nonatomic) IBOutlet UILabel *lblOtherSectionCell;
-(void) setUpTAbleViewWithStory:(Story *)story ofUser:(User *)user withIdentifier:(NSString *)identifier indexPath:(NSIndexPath *)indexpath;
@end
