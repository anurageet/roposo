//
//  storyCardTableViewCell.h
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/28/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Story.h"
#import "User.h"
#import "BtnSubclass.h"
@interface StoryCardTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;
@property (strong, nonatomic) IBOutlet UILabel *lblAUthor;

@property (strong, nonatomic) IBOutlet BtnSubclass *btnFollow;


- (IBAction)btnFollow:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;

-(void)story:(Story *)story postedBY:(User *) user;
@end
