//
//  storyCardTableViewCell.m
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/28/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import "StoryCardTableViewCell.h"
#import "DDData.h"

@implementation StoryCardTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnFollow:(BtnSubclass *)sender {
    DDData *obj = [[DDData alloc] init];
   NSArray *jsonDataArray = [obj reteriveData];
    NSMutableArray *arrSaved= [[NSMutableArray alloc] init];
    NSMutableDictionary *dic1;
    for (NSDictionary __strong *dic in jsonDataArray) {
        if ([sender.uniqueid isEqualToString:[dic objectForKey:@"id"]]) {
           dic1  = [dic mutableCopy];
            
            if ([[dic1 valueForKey:@"is_following"] boolValue]) {
              
                [dic1 setValue:[NSNumber numberWithBool:NO] forKey:@"is_following"];
                NSInteger  number = [[dic1 objectForKey:@"followers"] integerValue] -1;
                [dic1 setValue:[NSNumber numberWithInteger:number] forKey:@"followers"];
            }
            else
            {
            [dic1 setValue:[NSNumber numberWithBool:YES] forKey:@"is_following"];
            NSInteger  number = [[dic1 objectForKey:@"followers"] integerValue] +1;
            [dic1 setValue:[NSNumber numberWithInteger:number] forKey:@"followers"];
            }
            dic = dic1;
            
        }
        [arrSaved addObject:dic];
        
    }
    
   
    [obj saveData:arrSaved];
}

-(void)story:(Story *)story postedBY:(User *) user
{
    [self underLine];
    self.lblTitle.text= [NSString stringWithFormat:@"%@",story.stitle];
    self.lblDesc.text = [NSString stringWithFormat:@"%@",story.sdescription];
    self.lblAUthor.text = [NSString stringWithFormat:@"By %@",user.username];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",user.uimg]];
     [self.imgUser setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.btnFollow.uniqueid = user.uid;
}


-(void)underLine
{
    CAShapeLayer *line = [CAShapeLayer layer];
    UIBezierPath *linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint:CGPointMake(self.frame.origin.x , self.frame.origin.y + self.frame.size.height-1)];
    [linePath addLineToPoint:CGPointMake(self.frame.origin.x +self.frame.size.width, self.frame.origin.y + self.frame.size.height-1)];
    line.lineWidth = 1.0;
    line.path=linePath.CGPath;
    line.strokeColor = [UIColor
                         grayColor].CGColor;
    [[self layer] addSublayer:line];
}


@end
