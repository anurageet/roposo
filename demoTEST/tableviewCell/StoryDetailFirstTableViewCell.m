//
//  StoryDetail1TableViewCell.m
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/29/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import "StoryDetailFirstTableViewCell.h"
#import "DDData.h"
@implementation StoryDetailFirstTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnFollow:(BtnSubclass *)sender {
    DDData *obj = [[DDData alloc] init];
    NSArray *jsonDataArray = [obj reteriveData];
    NSMutableArray *arrSaved= [[NSMutableArray alloc] init];
    NSMutableDictionary *dic1;
    for (NSDictionary __strong *dic in jsonDataArray) {
        if ([sender.uniqueid isEqualToString:[dic objectForKey:@"id"]]) {
            dic1  = [dic mutableCopy];
            
            if ([[dic1 valueForKey:@"is_following"] boolValue]) {
                
                [dic1 setValue:[NSNumber numberWithBool:NO] forKey:@"is_following"];
                NSInteger  number = [[dic1 objectForKey:@"followers"] integerValue] -1;
                [dic1 setValue:[NSNumber numberWithInteger:number] forKey:@"followers"];
                 self.lblFolwers.text = [NSString stringWithFormat:@"%ld",(long)number];
            }
            else
            {
                [dic1 setValue:[NSNumber numberWithBool:YES] forKey:@"is_following"];
                NSInteger  number = [[dic1 objectForKey:@"followers"] integerValue] +1;
                [dic1 setValue:[NSNumber numberWithInteger:number] forKey:@"followers"];
                self.lblFolwers.text = [NSString stringWithFormat:@"%ld",(long)number];
            }
            dic = dic1;
            
        }
        [arrSaved addObject:dic];
        
    }
        [obj saveData:arrSaved];
}

- (IBAction)btnLike:(id)sender {
}

-(void) setUpTAbleViewWithStory:(Story *)story ofUser:(User *)user withIdentifier:(NSString *)identifier indexPath:(NSIndexPath *)indexpath
{
    
    
    if ([identifier isEqualToString:@"detailFirst"]) {
        self.lblHandler.text = user.uhandle;
        self.lblTitle.text = story.stitle;
        self.btnFollow.uniqueid = user.uid;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",story.simg]];
        [self.imgView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder"]];
        [self.imgView setClipsToBounds:YES];
        self.lblFolwers.text = [NSString stringWithFormat:@"%ld",(long)user.ufollowers];
    }
    else
    {
        if (indexpath.section == 1) {
            [self.lblOtherSectionCell setText:story.sverb];
        }
        else if (indexpath.section == 2)
        {
            [self.lblOtherSectionCell setText:story.sdescription];

        }
        else if (indexpath.section == 3)
        {
            [self.lblOtherSectionCell setText:story.surl];

        }
    }
}

@end
