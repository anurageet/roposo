//
//  Defines.h
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/28/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#ifndef Defines_h
#define Defines_h

#define CHECK_FOR_NULL(data)   (([data isKindOfClass:[NSNull class]]||data==nil)?@"":data)

#endif /* Defines_h */
