//
//  Story.h
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/28/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Story : NSObject
@property (nonatomic, strong) NSString *sdescription;
@property (nonatomic, strong) NSString *sid;
@property (nonatomic, strong) NSString *sverb;
@property (nonatomic, strong) NSString *stitle;
@property (nonatomic, strong) NSString *suid;
@property (nonatomic, strong) NSString *surl;
@property (nonatomic, strong) NSString *simg;
@property (nonatomic, assign) BOOL slike_flag;
@property (nonatomic, assign) NSInteger slikes_count;
@property (nonatomic, assign) NSInteger scomment_count;
@end


