//
//  User.h
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/28/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property (nonatomic, strong) NSString *uabout;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *uhandle;
@property (nonatomic, strong) NSString *uurl;
@property (nonatomic, strong) NSString *uimg;
@property (nonatomic, assign) BOOL uis_following;
@property (nonatomic, assign) NSInteger ufollowers;
@property (nonatomic, assign) NSInteger ufollowing;
@property (nonatomic, assign) NSInteger createdOn;

@end
/*
 
 "about": "Mother, actor, entrepreneur, fitness enthusiast and an eternal positive thinker",
 "id": "238bb4ca-606d-4817-afad-78bee2898264",
 "username": "Shilpa shetty kundra ",
 "followers": 35215,
 "following": 5,
 "image": "http://img.ropose.com/userImages/13632253306661657730401415143533525274225757_circle.png",
 
 "url": "http://www.roposo.com/profile/shilpa-shetty-kundra-/238bb4ca-606d-4817-afad-78bee2898264",
 "handle": "@shilpashettykundra",
 "is_following": false,
 "createdOn": 1439530320545
 */