//
//  storyCardViewController.m
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/28/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import "StoryCardViewController.h"
#import "StoryCardTableViewCell.h"
#import "Story.h"
#import "User.h"
#import "StoryDetailViewController.h"
#import "DDData.h"
@interface StoryCardViewController ()
{
    NSArray *jsonDataArray;
    DDData *objDDData;
}
@property (strong, nonatomic) IBOutlet UITableView *tblViewStoryCard;
@property (strong, nonatomic) NSArray *arrData;
@property (strong, nonatomic) NSMutableArray *arrUser,*arrStory;

@end

@implementation StoryCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.tblViewStoryCard.estimatedRowHeight = 50.0;
    objDDData = [[DDData alloc] init];
    self.tblViewStoryCard.rowHeight = UITableViewAutomaticDimension;
    self.navigationController.title = @"Story list";
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"iOS-Data" ofType:@"json"];
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
   // NSString *data = .... // your json representation
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"myData.json"];
    [myJSON writeToFile:appFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
    // Do any additional setup after loading the view from its nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    jsonDataArray = [objDDData reteriveData];
    [self refineData:jsonDataArray];
}


#pragma mark :- Custome Method



-(void)refineData:(NSArray *)arrComplete
{
    self.arrUser = [[NSMutableArray alloc ] init];
    self.arrStory = [[NSMutableArray alloc ] init];
    for (NSDictionary *dic in arrComplete) {
        if ([dic objectForKey:@"username"]) {
            User *objUser = [[User alloc] init];
            objUser.uabout = [dic objectForKey:@"about"];
            objUser.uid = [dic objectForKey:@"id"];
            objUser.username = [dic objectForKey:@"username"];
            objUser.ufollowers = [[dic objectForKey:@"followers"] integerValue];
            objUser.ufollowing = [[dic objectForKey:@"following"] integerValue];
            objUser.uimg = [dic objectForKey:@"image"];
            objUser.uurl = [dic objectForKey:@"url"];
            objUser.uis_following = [[dic objectForKey:@"is_following"] boolValue];
            objUser.uhandle = [dic objectForKey:@"handle"];
            objUser.createdOn = [[dic objectForKey:@"createdOn"] integerValue];
            [self.arrUser addObject:objUser];
        }
        else
        {
            Story *objStory = [[Story alloc] init];
            objStory.sdescription = [dic objectForKey:@"description"];
            objStory.sid = [dic objectForKey:@"id"];
            objStory.sverb = [dic objectForKey:@"verb"];
            objStory.slikes_count = [[dic objectForKey:@"likes_count"] integerValue];
            objStory.scomment_count = [[dic objectForKey:@"comment_count"] integerValue];
            objStory.stitle = [dic objectForKey:@"title"];
            objStory.surl = [dic objectForKey:@"url"];
            objStory.slike_flag = [[dic objectForKey:@"like_flag"] boolValue];
            objStory.suid = [dic objectForKey:@"db"];
            objStory.simg = [dic objectForKey:@"si"] ;
            [self.arrStory addObject:objStory];
        }
    }
}

-(User *)associatedUser:(NSString *)userID
{
    User *associatUser;
    for (User *obj in self.arrUser) {
        if ([obj.uid isEqualToString:userID])
            associatUser = obj;
    }
    
    
    return associatUser;
}


#pragma mark :- TABLEVIEW DATA SOURCE AND DELEGATE


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrStory count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *str = @"stroryCardCell";
    
    StoryCardTableViewCell *objstoryCardTableViewCell = (StoryCardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:str];
    if (!objstoryCardTableViewCell) {
        objstoryCardTableViewCell = (StoryCardTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"StoryCardTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
    [objstoryCardTableViewCell story:[self.arrStory objectAtIndex:indexPath.row] postedBY:[self associatedUser:((Story *)[self.arrStory objectAtIndex:indexPath.row]).suid]];
   
    [objstoryCardTableViewCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return objstoryCardTableViewCell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StoryDetailViewController *obj = [[StoryDetailViewController alloc] initWithNibName:@"StoryDetailViewController" bundle:nil];
    jsonDataArray = [objDDData reteriveData];
    [self refineData:jsonDataArray];
    obj.story = (Story *)[self.arrStory objectAtIndex:indexPath.row];
    obj.user = [self associatedUser:obj.story.suid];
    [self.navigationController pushViewController:obj animated:YES];
}

@end
