//
//  storyCardViewController.h
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/28/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryCardViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@end
