//
//  StoryDetailViewController.h
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/28/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Story.h"
#import "User.h"
@interface StoryDetailViewController : UIViewController <UITableViewDataSource ,UITableViewDelegate>
@property(nonatomic,strong) Story *story;
@property(nonatomic,strong) User *user;
@end
