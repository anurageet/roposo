//
//  StoryDetailViewController.m
//  demoTEST
//
//  Created by Anurag Bhakuni on 4/28/16.
//  Copyright © 2016 osscube. All rights reserved.
//

#import "StoryDetailViewController.h"
#import "StoryDetailFirstTableViewCell.h"

@interface StoryDetailViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tblStoryDetailView;
@property (strong, nonatomic) NSArray *arrSection;
@end

@implementation StoryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.arrSection = @[@"Verb",@"Description",@"More Info."];
    self.tblStoryDetailView.estimatedRowHeight = 50.0;
    self.tblStoryDetailView.rowHeight = UITableViewAutomaticDimension;
}
#pragma mark :- TAbleView DataSource and Delegate
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section ==0) {
        return @"";
    }
    return [self.arrSection objectAtIndex:section-1];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section ==0) {
        return 0;
    }
    else
        return 40;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.arrSection count] +1 ;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return 1;
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *str = @"detailFirst";
    static NSString *str1 = @"detailSecond";
    
    StoryDetailFirstTableViewCell *objStoryDetailFirstTableViewCell;
    if (!objStoryDetailFirstTableViewCell) {
        
        if (indexPath.section==0) {
             objStoryDetailFirstTableViewCell = (StoryDetailFirstTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"StoryDetailFirstTableViewCell" owner:self options:nil] objectAtIndex:0];
            
             [objStoryDetailFirstTableViewCell setUpTAbleViewWithStory:self.story ofUser:self.user withIdentifier:str indexPath:indexPath];
        }
        else
        objStoryDetailFirstTableViewCell = (StoryDetailFirstTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"StoryDetailOtherTableViewCell" owner:self options:nil] objectAtIndex:0];
        
         [objStoryDetailFirstTableViewCell setUpTAbleViewWithStory:self.story ofUser:self.user withIdentifier:str1 indexPath:indexPath];
    }
    
   
    
    [objStoryDetailFirstTableViewCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return objStoryDetailFirstTableViewCell;}

@end
